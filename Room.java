package houseProgramMain;

public class Room {
	
	//Variables
	public static String wallColor;
	public static String floorType;
	public static int windows;
	public static int roomNumber;
	
	//Constructors
	
	public Room() {
		wallColor = "";
		floorType = "";
		windows = 0;
		roomNumber = 0;
	}
	
    public void setWallColor(String wallColor) {
    	this.wallColor = wallColor;
    }
    public void setFloorType(String floorType) {
    	this.floorType = floorType;
    }
    public void setWindows(int windows) {
    	this.windows = windows;
    }
    public void setRoomNumber(int roomNumber) {
    	this.roomNumber = roomNumber;
    }

    public String getWallColor() {
    	return this.wallColor;
    }        
    public String getFloorType() {
    	return this.floorType;
    }        
    public int getWindows() {
    	return this.windows;
    }
    public int getRoomNumber() {
    	return this.roomNumber;
    }

    @Override
	public String toString() {
		return (wallColor + floorType + windows + roomNumber + getWallColor() + getFloorType() + getWindows()
				+ getRoomNumber());
	}
}
